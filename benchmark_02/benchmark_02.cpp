// mylib
#include "../mylibrary/shellsort.h"

// stl
#include <chrono>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <random>
#include <algorithm>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;





///
///
///   This program benchmarks run times when sorting
///
///

using TimingType = size_t;

template <typename Elem>
using SizeType = typename std::vector<Elem>::size_type;



template <typename Container>
void 
writeToDat( Container C, const std::string& benchmarkname ) {

  // Dump data to file of the format size in aending order
  // size time
  // s0   t0
  // s1   t1
  // s2   t2

  // Build output data
  std::ofstream ofs;
  ofs.exceptions( std::ofstream::failbit | std::ofstream::badbit );

  ofs.open( "benchmark_" + benchmarkname + ".dat", std::ofstream::out | std::ofstream::trunc );
  ofs << "size time" << std::endl;
  for( const auto& data : C)
    ofs << data.size << " " << data.duration << std::endl;
  ofs.close();
}






template <typename Elem>
std::vector<Elem> 
genDataTestSet( typename std::vector<Elem>::size_type n ) {

  std::vector<Elem> C;

  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<Elem> dist( 
    std::numeric_limits<Elem>::min(), 
    std::numeric_limits<Elem>::max()); 

  C.reserve(n);
  for( typename std::vector<Elem>::size_type i {0}; i < n; ++i )
    C.emplace_back(dist(mt));

  return C;
}





//template <class RandomAccessIterator, class Compare = std::less<typename RandomAccessIterator::value_type> > 
//using SortFn = void (*)(RandomAccessIterator,RandomAccessIterator,Compare);

template <class Container>
TimingType
time_std_sort( Container C ) {

  using namespace mylib;

  auto start = std::chrono::high_resolution_clock::now();
  std::sort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <class Container>
TimingType
time_mylib_bubble( Container C ) {

  using namespace mylib;

  auto start = std::chrono::high_resolution_clock::now();
  mylib::bubbleSort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <class Container>
TimingType
time_mylib_shell( Container C ) {

  using namespace mylib;

  auto start = std::chrono::high_resolution_clock::now();
  mylib::shellSort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}


template <typename Elem>
struct TimingData {
  SizeType<Elem>    size;
  TimingType        duration;

  TimingData( const SizeType<Elem>& s, const TimingType& d ) 
    : size{s}, duration{d} {}
};


int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)

  using ElementType   = int;
  
  std::vector<SizeType<ElementType>> fill_sizes {
    20
    ,100
    ,1000
    ,10000
    ,20000
    ,30000
    ,40000
    ,50000
    ,100000
//    ,1000000                  // 4MB
//    ,10000000
//    ,100000000
//    ,1000000000               // 4GB (given element type is 4B)
  };


  std::cout << "Benchmarking sorting!" << std::endl;

  std::vector<TimingData<ElementType>> timing_data_std_sort;
  std::vector<TimingData<ElementType>> timing_data_mylib_bubble;
  std::vector<TimingData<ElementType>> timing_data_mylib_shell;
  for( const auto& fill_size : fill_sizes ) {

    std::cout << "  Generating data set N = " << fill_size << std::endl;
    auto C = genDataTestSet<int>(fill_size);

    auto C_std = C;
    auto C_bubble = C;
    auto C_shell = C;

    // Time std::sort
    std::cout << "    Timing std::sort..." << std::endl;
    timing_data_std_sort.push_back( 
      TimingData<ElementType>(fill_size, 
        time_std_sort(C_std) 
      ) 
    );

    // Time mylib::bubbleSort
    std::cout << "    Timing mylib::bubbleSort..." << std::endl;
    timing_data_mylib_bubble.push_back( 
      TimingData<ElementType>(fill_size, 
        time_mylib_bubble(C_std) 
      ) 
    );

    // Time mylib::shellSort
    std::cout << "    Timing mylib::shellSort..." << std::endl;
    timing_data_mylib_shell.push_back( 
      TimingData<ElementType>(fill_size, 
        time_mylib_shell(C_std) 
      ) 
    );
  }
 

  // Write to file
  writeToDat(timing_data_std_sort,     "stl_sort");
  writeToDat(timing_data_mylib_bubble, "mylib_bubble");
  writeToDat(timing_data_mylib_shell,  "mylib_shell");



  return 0;
}
catch (std::ifstream::failure e) {
  std::cerr << "Exception opening/reading/closing file: " << e.what();
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}

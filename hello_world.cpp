// mylib
#include "mylibrary/functions.h"
#include "mylibrary/list.h"

// stl
#include <iostream>
#include <stdexcept>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;

int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)

  mylib::helloWorld( "... anywho ..." );
  mylib::helloWorld("Can I help u");


  //list_item * head = nullptr;
  //list_item * last = nullptr;
  mylib::List list{2,3,4};
  list.Push_Back(1);
  list.Push_front(7);
  list.display();

  //Front()
  std::shared_ptr<mylib::list_item> abc = list.Front();
  std::cout<<"\nAddress of First Element = "<<abc<<"\n";

  //Back
  std::shared_ptr<mylib::list_item> xyz = list.Back();
  std::cout<<"\nAddress of Last Element = "<<xyz<<"\n";

  //Begin
  std::cout<<"\nValue of First Element using Begin() = "<<list.Begin()<<"\n";

  //End
  std::cout<<"\nValue of Last Element using End() = "<<list.End()<<"\n";


  return 0;
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}


#include <gtest/gtest.h>

#include "../list.h"



TEST(Container_List,Size_functionality) {


  using namespace mylib;


  List list { 12,1,2,3,4,6,7,8,9 };


  EXPECT_EQ(9,list.size());

}
